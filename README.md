# Documentation for OCR and Image Processing Script

This document provides a comprehensive guide on how to use the provided R script for Optical Character Recognition (OCR) and image processing. The script uses several R libraries including `tidyverse`, `stringr`, `tesseract`, and `magick` to manipulate images and extract text.

## Overview

The script is divided into sections that sequentially prepare the image, apply OCR, and process the text output. Each section is designed to build upon the previous steps to improve the accuracy of text extraction.

### Initial Setup and OCR Application

1. **Loading Libraries:** The script starts by loading essential R libraries needed for data manipulation, string operations, OCR, and image processing.
2. **Reading and Displaying the Image:** An image file is read into the environment using `magick`, and its properties are displayed.
3. **Initial OCR Application:** OCR is applied to the unprocessed image to establish a baseline for text recognition quality.
4. **Output Display:** The OCR output is displayed, and the text is split into lines to check the initial readability.

### Image Preprocessing for Enhanced OCR

5. **Grayscale Conversion:** The image is converted to grayscale to reduce complexity, which can help in enhancing the accuracy of subsequent OCR.
6. **Threshold Application:** A binary threshold is applied to simplify the image further, making it easier for the OCR engine to discern text.
7. **Vertical Line Removal:** Vertical lines are removed using morphological operations to prevent them from interfering with text recognition.
8. **Image Cropping:** The image is cropped to focus on relevant areas, further refining the input for better OCR results.
9. **Re-application of OCR:** OCR is applied again to the processed image to evaluate improvements in text recognition.

### Data Extraction and Final Processing

10. **Text Extraction:** Regular expressions are used to extract specific types of text, such as names of facilities, from the OCR results.
11. **Number-Specific OCR Configuration:** The OCR engine is configured to recognize numbers only, enhancing the accuracy for numerical data extraction.
12. **Second Example Processing:** The script includes steps to process a second image, applying similar preprocessing techniques to handle different image characteristics.

### Advanced Text and Data Handling

13. **Function Definition for Text Splitting:** A custom function is defined to split text into words and numbers, organizing the data into a structured format.
14. **Data Table Creation:** The extracted data is compiled into a data table, applying the custom function to format and arrange the text correctly.
15. **Data Inspection and Saving:** The final data table is inspected using a viewer and saved to a CSV file for further use.

## Usage Instructions

- **Prerequisites:** Ensure all mentioned libraries are installed in your R environment.
- **Execution:** Run the script in sections to observe each step's effect on the image and OCR output. Adjust parameters like thresholds and cropping dimensions based on specific image characteristics.
- **Modification:** Modify the regular expressions and the custom text splitting function as needed to suit different types of textual data in images.

